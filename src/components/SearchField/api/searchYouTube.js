import axios from 'axios'


export default function searchYouTube(q) {
	const apiPromise = new Promise((resolve, reject) => {
		const baseURL = process.env.VUE_APP_YOUTUBE_API_BASE
		const apiKey = process.env.VUE_APP_GOOGLE_API_KEY

		axios
			.get(`${baseURL}?part=snippet&order=viewCount&q=${q?.queryString}&maxResults=${q?.maxResults || 10}&key=${apiKey}`)
			.then(res => {
				console.log('[API:: searchYouTube]:: response', res)
				resolve(res)
			})
			.catch(error => {
				console.log('[API:: searchYouTube]:: error', error)
				reject(error)
			});
	});
	return apiPromise
}
