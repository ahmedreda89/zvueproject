import searchYouTube from '@/components/SearchField/api/searchYouTube'

// define app store actions names
export const ACTION_SEARCH_YT_VIDEOS = 'actionSearchYtVideos'
export const ACTION_SEARCH_YT_VIDEOS_COMPLETED = 'actionSearchYtVideosCompleted'
export const ACTION_SEARCH_YT_VIDEOS_FAILED = 'actionSearchYtVideosFailed'
export const ACTION_FLUSH_SEARCH_RESULTS = 'actionFlushSearchResults'

// define app store mutations names
const GET_VIDEOS = 'getVideos'
const GET_VIDEOS_COMPLETED = 'getVideosCompleted'
const GET_VIDEOS_FAILED = 'getVideosFailed'
const FLUSH_VIDEOS = 'flushVideos'

// initial app state
const state = {
	videos: {
		list: [],
		meta: {}
	},
}

const getters = {
	getVideos (state) {
		return state.videos
	}
}

// app store actions
// todo: separate in a file
const actions = {
	[ACTION_SEARCH_YT_VIDEOS] ({ commit }, q) {
		commit(GET_VIDEOS);

		searchYouTube(q)
			.then((res) => {
				commit(GET_VIDEOS_COMPLETED, res)
			})
			.catch((err) => {
				commit(GET_VIDEOS_FAILED, err)
			})
	},
	[ACTION_SEARCH_YT_VIDEOS_COMPLETED] ({ commit }) {
		commit(GET_VIDEOS_COMPLETED)
	},
	[ACTION_SEARCH_YT_VIDEOS_FAILED] ({ commit }) {
		commit(GET_VIDEOS_FAILED)
	},
	[ACTION_FLUSH_SEARCH_RESULTS] ({ commit }) {
		commit(FLUSH_VIDEOS)
	}
}

// app store mutations
// This is equivalent to the reducer in redux observable
// todo: separate in a file
const mutations = {
	[GET_VIDEOS] (state) {
		state.videos.meta = {
			isLoading: true,
			isCompleted: false,
			isFailed: false,
		}

		console.log('[store]mutations:: getVideos', state.videos)
	},
	[GET_VIDEOS_COMPLETED] (state, payload) {
		state.videos = {
			list: payload?.data?.items,
			meta: {
				isLoading: false,
				isCompleted: true,
				isFailed: false,
				totalCount: payload?.data?.pageInfo?.totalResults,
			},
		}

		console.log('[store]mutations:: getVideosCompleted', state.videos)
	},
	[GET_VIDEOS_FAILED] (state, err) {
		state.videos = {
			list: [],
			meta: {
				isLoading: false,
				isCompleted: false,
				isFailed: true,
				error: err,
			},
		}

		console.log('[store]mutations:: getVideosFailed', state.videos)
		console.log('[store]mutations:: getVideosFailed:: error', err)
	},
	[FLUSH_VIDEOS] (state) {
		state.counter = {
			list: [],
			meta: {}
		},

		console.log('[store]mutations:: flushVideos'+ state.videos)
	}
}

export default {
	state,
	actions,
	mutations,
	getters
}